import {
  BrowserRouter,
  Routes,
  Route
} from "react-router-dom";

//local user_related_compons imports
import { SignUp } from './compons/user_realated_compons/SignUp/Signup';
import Login from './compons/user_realated_compons/Login/Login';
import FotgotPassword from "./compons/user_realated_compons/ForgotPassword/FotgotPassword";
import UtubeHome from "./compons/utube_home/UtubeHome";
import UtubeTrendingPage from "./compons/utube_trending_page/UtubeTrendingPage";
import UtubeExplore from "./compons/utube_explore/UtubeExplore";
import UtubePlayerPage from "./compons/utube_player_page/UtubePlayerPage";


function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element = {<UtubeHome />} />
        <Route path="/signup" element = {<SignUp />} />
        <Route path="/login" element = {<Login />} />
        <Route path="/forgotpassword" element = {<FotgotPassword />} />
        <Route path="/trending" element = {<UtubeTrendingPage />} />
        <Route path="/explore" element = {<UtubeExplore />} />
        <Route path="player" element = {<UtubePlayerPage />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
