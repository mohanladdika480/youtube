import React, { Component } from "react";
import "./UtubeHome.css";

class UtubeHome extends Component {
  render() {
    return (
      <>
        <div className="topbar">
          <div className="left">
            <i className="fas fa-bars" />
            <div className="logo">
        <img src="image.png" class="pl-logo" />
        <span class="logo-title"> My Tube </span>
            </div>
          </div>
          <div className="middle">
            <div className="search">
              <input type="text" name="" id="" placeholder="Search" />
              <div className="searchbtn">
                <i className="fa fa-search" />
              </div>
            </div>
            <i className="fas fa-microphone" />
          </div>
          <div className="right">
            <i className="fas fa-video" />
            <i className="fas fa-th" />
            <i className="fas fa-bell" />
            <img className="profile" src="https://picsum.photos/200" alt="" />
          </div>
        </div>
        <div className="sidebar">
          <div className="section">
            <ul>
              <li className="active">
                {" "}
                <i className="fas fa-home" /> Home
              </li>
              <li>
                <i className="fas fa-compass" />
                Explore
              </li>
              <li>
                <i className="fab fa-youtube-square" />
                Subscriptions
              </li>
            </ul>
          </div>
          <div className="section">
            <ul>
              <li>
                <i className="fas fa-play" />
                Library
              </li>
              <li>
                <i className="fas fa-history" />
                History
              </li>
              <li>
                <i className="fab fa-youtube" />
                Your videos
              </li>
            </ul>
          </div>
        </div>
        <div className="home_content">
          <div className="d-flex flex-row">
            <div className="vid">
              <div className="preview">
                <iframe
                  width={220}
                  height={170}
                  src="https://www.youtube.com/embed/Hs4NwTd49QA"
                  title="YouTube video player"
                  frameBorder={0}
                  allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                  allowFullScreen="true"
                />
                <div className="time">1:48</div>
              </div>
              <div className="info">
                <div className="left">
                  <img className="profile" src="https://picsum.photos/201" />
                  <div className="below">
                    <h2>A YouTube Video</h2>
                    <p className="username">user12345</p>
                    <p>934 views • 2 days ago</p>
                  </div>
                </div>
                <i className="fas fa-ellipsis-v" />
              </div>
            </div>
            <div className="vid">
              <div className="preview">
                <iframe
                  width={220}
                  height={170}
                  src="https://www.youtube.com/embed/N6xD_ZytY2A"
                  title="YouTube video player"
                  frameBorder={0}
                  allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                  allowFullScreen="true"
                />
                <div className="time">1:48</div>
                <div className="hovicons">
                  <i className="fas fa-clock" />
                  <i className="fas fa-stream" />
                </div>
              </div>
              <div className="info">
                <div className="left">
                  <img className="profile" src="https://picsum.photos/201" />
                  <div className="below">
                    <h2>A YouTube Video</h2>
                    <p className="username">user12345</p>
                    <p>934 views • 2 days ago</p>
                  </div>
                </div>
                <i className="fas fa-ellipsis-v" />
              </div>
            </div>
            <div className="vid">
              <div className="preview">
                <iframe
                  width={220}
                  height={170}
                  src="https://www.youtube.com/embed/QE3OonGXDXQ"
                  title="YouTube video player"
                  frameBorder={0}
                  allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                  allowFullScreen="true"
                />
                <div className="time">1:48</div>
              </div>
              <div className="info">
                <div className="left">
                  <img className="profile" src="https://picsum.photos/201" />
                  <div className="below">
                    <h2>A YouTube Video</h2>
                    <p className="username">user12345</p>
                    <p>934 views • 2 days ago</p>
                  </div>
                </div>
                <i className="fas fa-ellipsis-v" />
              </div>
            </div>
            <div className="vid">
              <div className="preview">
                <iframe
                  width={220}
                  height={170}
                  src="https://www.youtube.com/embed/2sxEsG64CsY"
                  title="YouTube video player"
                  frameBorder={0}
                  allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                  allowFullScreen="true"
                />
              </div>
              <div className="info">
                <div className="left">
                  <img className="profile" src="https://picsum.photos/201" />
                  <div className="below">
                    <h2>A YouTube Video</h2>
                    <p className="username">user12345</p>
                    <p>934 views • 2 days ago</p>
                  </div>
                </div>
                <i className="fas fa-ellipsis-v" />
              </div>
            </div>
          </div>
        </div>
        <div className="content_bottom">
          <div className="d-flex flex-row">
            <div className="vid">
              <div className="preview">
                <iframe
                  width={220}
                  height={170}
                  src="https://www.youtube.com/embed/wF_B_aagLfI"
                  title="YouTube video player"
                  frameBorder={0}
                  allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
									allowFullScreen="true"
                />
                <div className="time">1:48</div>
              </div>
              <div className="info">
                <div className="left">
                  <img className="profile" src="https://picsum.photos/201" />
                  <div className="below">
                    <h2>A YouTube Video</h2>
                    <p className="username">user12345</p>
                    <p>934 views • 2 days ago</p>
                  </div>
                </div>
                <i className="fas fa-ellipsis-v" />
              </div>
            </div>
            <div className="vid">
              <div className="preview">
                <iframe
                  width={220}
                  height={170}
                  src="https://www.youtube.com/embed/wF_B_aagLfI"
                  title="YouTube video player"
                  frameBorder={0}
                  allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                  allowFullScreen="true"
                />
                <div className="time">1:48</div>
              </div>
              <div className="info">
                <div className="left">
                  <img className="profile" src="https://picsum.photos/201" />
                  <div className="below">
                    <h2>A YouTube Video</h2>
                    <p className="username">user12345</p>
                    <p>934 views • 2 days ago</p>
                  </div>
                </div>
                <i className="fas fa-ellipsis-v" />
              </div>
            </div>
            <div className="vid">
              <div className="preview">
                <iframe
                  width={220}
                  height={170}
                  src="https://www.youtube.com/embed/wF_B_aagLfI"
                  title="YouTube video player"
                  frameBorder={0}
                  allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                  allowFullScreen="true"
                />
                <div className="time">1:48</div>
              </div>
              <div className="info">
                <div className="left">
                  <img className="profile" src="https://picsum.photos/201" />
                  <div className="below">
                    <h2>A YouTube Video</h2>
                    <p className="username">user12345</p>
                    <p>934 views • 2 days ago</p>
                  </div>
                </div>
                <i className="fas fa-ellipsis-v" />
              </div>
            </div>
            <div className="vid">
              <div className="preview">
                <iframe
                  width={220}
                  height={170}
                  src="https://www.youtube.com/embed/wF_B_aagLfI"
                  title="YouTube video player"
                  frameBorder={0}
                  allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                  allowFullScreen="true"
                />
              </div>
              <div className="info">
                <div className="left">
                  <img className="profile" src="https://picsum.photos/201" />
                  <div className="below">
                    <h2>A YouTube Video</h2>
                    <p className="username">user12345</p>
                    <p>934 views • 2 days ago</p>
                  </div>
                </div>
                <i className="fas fa-ellipsis-v" />
              </div>
            </div>
          </div>
        </div>
        <div className="content_bottom">
          <div className="d-flex flex-row">
            <div className="vid">
              <div className="preview">
                <iframe
                  width={220}
                  height={170}
                  src="https://www.youtube.com/embed/wF_B_aagLfI"
                  title="YouTube video player"
                  frameBorder={0}
                  allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                  allowFullScreen="true"
                />
                <div className="time">1:48</div>
              </div>
              <div className="info">
                <div className="left">
                  <img className="profile" src="https://picsum.photos/201" />
                  <div className="below">
                    <h2>A YouTube Video</h2>
                    <p className="username">user12345</p>
                    <p>934 views • 2 days ago</p>
                  </div>
                </div>
                <i className="fas fa-ellipsis-v" />
              </div>
            </div>
            <div className="vid">
              <div className="preview">
                <iframe
                  width={220}
                  height={170}
                  src="https://www.youtube.com/embed/2sxEsG64CsY"
                  title="YouTube video player"
                  frameBorder={0}
                  allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                  allowFullScreen="true"
                />
                <div className="time">1:48</div>
              </div>
              <div className="info">
                <div className="left">
                  <img className="profile" src="https://picsum.photos/201" />
                  <div className="below">
                    <h2>A YouTube Video</h2>
                    <p className="username">user12345</p>
                    <p>934 views • 2 days ago</p>
                  </div>
                </div>
                <i className="fas fa-ellipsis-v" />
              </div>
            </div>
            <div className="vid">
              <div className="preview">
                <iframe
                  width={220}
                  height={170}
                  src="https://www.youtube.com/embed/2sxEsG64CsY"
                  title="YouTube video player"
                  frameBorder={0}
                  allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                  allowFullScreen="true"
                />
                <div className="time">1:48</div>
              </div>
              <div className="info">
                <div className="left">
                  <img className="profile" src="https://picsum.photos/201" />
                  <div className="below">
                    <h2>A YouTube Video</h2>
                    <p className="username">user12345</p>
                    <p>934 views • 2 days ago</p>
                  </div>
                </div>
                <i className="fas fa-ellipsis-v" />
              </div>
            </div>
            <div className="vid">
              <div className="preview">
                <iframe
                  width={220}
                  height={170}
                  src="https://www.youtube.com/embed/2sxEsG64CsY"
                  title="YouTube video player"
                  frameBorder={0}
                  allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                  allowFullScreen="true"
                />
              </div>
              <div className="info">
                <div className="left">
                  <img className="profile" src="https://picsum.photos/201" />
                  <div className="below">
                    <h2>A YouTube Video</h2>
                    <p className="username">user12345</p>
                    <p>934 views • 2 days ago</p>
                  </div>
                </div>
                <i className="fas fa-ellipsis-v" />
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default UtubeHome;
