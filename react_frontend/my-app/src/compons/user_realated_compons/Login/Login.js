import React, { Component } from 'react';
import "./Login.css"

class Login extends Component {
    render() {
        return (
            <div className="content">
                <div className="box">
                <div className="header">
                    <h1>Log In</h1>
                </div>
                <div className="email container">
                    <input type="email" placeholder="Enter your email" />
                </div>
                <p className="Log_in_warn">*Email doesn't exist</p>
                <div className="passsword container">
                    <input type="password" placeholder="password" />
                </div>
                <p className="Log_in_warn">*Password doesn't match</p>
                <div>
                    <a href="#">Forgot password?</a>
                    <a></a>
                </div>
                <a>
                    <div>
                    <span>
                        <button> Create Account </button>
                    </span>
                    <span>
                        <button> Next </button>
                    </span>
                    </div>
                </a>
                </div>
            </div>
        );
    }
}

export default Login;