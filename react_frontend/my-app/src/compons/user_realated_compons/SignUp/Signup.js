import { Component } from "react";
import "./Signup.css";

class SignUp extends Component {
    render() {
        return (
            <div className="signup_content">
            <div className="signup_box">
              <div className="signup_header">
                <h1>Sign Up</h1>
              </div>
              <div className="signup_container">
                <input type="email" placeholder="First Name" />
              </div>
              <div className="signup_container">
                <input type="email" placeholder="Last Name" />
              </div>
              <div className="signup_container">
                <input type="email" placeholder="Enter Your Mail" />
              </div>
              <p className="signup_Log_in_warn">*Email Already in use.</p>
              <div className="signup_container">
                <input type="password" placeholder="Enter Password" />
              </div>
              <div className="signup_container">
                <input type="password" placeholder="Re-Enter Password" />
              </div>
              <p className="signup_Log_in_warn">*Password doesn't match.</p>
              {/* <div className="passsword signup_container">
                <input type="email" placeholder="Enter OTP" />
              </div>
              <p className="signup_Log_in_warn">*OTP is invalid</p> */}
              <div>
                <span>
                  <button > Log In </button>
                </span>
                <span>
                  <button> Next </button>
                </span>
              </div>
            </div>
          </div>
        )
    }
}

export {SignUp}