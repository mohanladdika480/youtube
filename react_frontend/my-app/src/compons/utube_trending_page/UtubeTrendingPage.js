import React, { Component } from 'react';
import "./UtubeTrendingPage.css";

class UtubeTrendingPage extends Component {
    render() {
        return (
            <>
            <div className="topbar">
              <div className= "left">
                <i className="fas fa-bars" />
                <div className="logo">
        <img src="image.png" class="pl-logo" />
        <span class="logo-title"> My Tube </span>
               </div>
              </div>
              <div className="middle">
                <div className="search">
                  <input type="text" name="" id="" placeholder="Search" />
                  <div className="searchbtn">
                    <i className="fa fa-search" />
                  </div>
                </div>
                <i className="fas fa-microphone" />
              </div>
              <div className="right">
                <i className="fas fa-video" />
                <i className="fas fa-th" />
                <i className="fas fa-bell" />
                <img className="profile" src="https://picsum.photos/200" alt="" />
              </div>
            </div>
            <div className="sidebar">
              <div className="section">
                <ul>
                  <li>
                    {" "}
                    <i className="fas fa-home" /> Home
                  </li>
                  <li className="active">
                    <i className="fas fa-compass" />
                    Explore
                  </li>
                  <li>
                    <i className="fab fa-youtube-square" />
                    Subscriptions
                  </li>
                </ul>
              </div>
              <div className="section">
                <ul>
                  <li>
                    <i className="fas fa-play" />
                    Library
                  </li>
                  <li>
                    <i className="fas fa-history" />
                    History
                  </li>
                  <li>
                    <i className="fab fa-youtube" />
                    Your videos
                  </li>
                </ul>
              </div>
            </div>
            <div className="tr-content">
              <div className="d-flex flex-row">
                <h1 className="tr-page-content"> Trending </h1>
              </div>
              <div className="d-flex flex-row">
                <div className="tr-vid tr-preview">
                  <iframe
                    width={220}
                    height={170}
                    src="https://www.youtube.com/embed/HRD2-_bU4K0"
                    title="YouTube video player"
                    frameBorder={0}
                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                    allowFullScreen=""
                  />
                  <div className="tr-info">
                    <div className="tr-video_text">
                      <h1 className="tr-video_heading">
                        #TilluAnnaDJPedithe Full Video Song |DJ Tillu Songs |Siddhu,Neha
                        Shetty |Vimal Krishna |Ram Miriyala
                      </h1>
                      <h1 className="tr-para_text">
                        {" "}
                        Aditya Music 2.5M views 13 hours ago{" "}
                      </h1>
                      <h1 className="tr-video_bottom_text">
                        {" "}
                        Watch &amp; Enjoy Tillu Anna DJ Pedithe Full Video Song From DJ
                        Tillu Movie. #DJTillu #TilluAnnaDJPedithe #RamMiriyala #Siddhu
                        #NehaShetty #VimalKrishna Singer: Ram Miriyala Lyrics: Kasarla...
                      </h1>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="tr-content_bottom">
              <div className="d-flex flex-row">
                <div className="tr-vid tr-preview">
                  <iframe
                    width={220}
                    height={170}
                    src="https://www.youtube.com/embed/M-954V9LORI"
                    title="YouTube video player"
                    frameBorder={0}
                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                    allowFullScreen="true"
                  />
                  <div className="tr-info">
                    <div className="tr-video_text">
                      <h1 className="tr-video_heading">
                        #TilluAnnaDJPedithe Full Video Song |DJ Tillu Songs |Siddhu,Neha
                        Shetty |Vimal Krishna |Ram Miriyala
                      </h1>
                      <h1 className="tr-para_text">
                        {" "}
                        Aditya Music 2.5M views 13 hours ago{" "}
                      </h1>
                      <h1 className="tr-video_bottom_text">
                        {" "}
                        Watch &amp; Enjoy Tillu Anna DJ Pedithe Full Video Song From DJ
                        Tillu Movie. #DJTillu #TilluAnnaDJPedithe #RamMiriyala #Siddhu
                        #NehaShetty #VimalKrishna Singer: Ram Miriyala Lyrics: Kasarla...
                      </h1>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="tr-content_bottom">
              <div className="d-flex flex-row">
                <div className="tr-vid tr-preview">
                  <iframe
                    width={220}
                    height={170}
                    src="https://www.youtube.com/embed/yGUKYKSJJc8"
                    title="YouTube video player"
                    frameBorder={0}
                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                    allowFullScreen="true"
                  />
                  <div className="tr-info">
                    <div className="tr-video_text">
                      <h1 className="tr-video_heading">
                        Tum Tum - Lyric Video | Enemy (Tamil) | Vishal,Arya | Anand
                        Shankar | Vinod Kumar | Thaman S
                      </h1>
                      <h1 className="tr-para_text"> Divo Music 146M views 3 months ago </h1>
                      <h1 className="tr-video_bottom_text">
                        {" "}
                        Watch &amp; Enjoy Tillu Anna DJ Pedithe Full Video Song From DJ
                        Tillu Movie. #DJTillu #TilluAnnaDJPedithe #RamMiriyala #Siddhu
                        #NehaShetty #VimalKrishna Singer: Ram Miriyala Lyrics: Kasarla...
                      </h1>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="tr-content_bottom">
              <div className="d-flex flex-row">
                <div className="tr-vid tr-preview">
                  <iframe
                    width={220}
                    height={170}
                    src="https://www.youtube.com/embed/yGUKYKSJJc8"
                    title="YouTube video player"
                    frameBorder={0}
                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                    allowFullScreen="true"
                  />
                  <div className="tr-info">
                    <div className="tr-video_text">
                      <h1 className="tr-video_heading">
                        Tum Tum - Lyric Video | Enemy (Tamil) | Vishal,Arya | Anand
                        Shankar | Vinod Kumar | Thaman S
                      </h1>
                      <h1 className="tr-para_text"> Divo Music 146M views 3 months ago </h1>
                      <h1 className="tr-video_bottom_text">
                        {" "}
                        Watch &amp; Enjoy Tillu Anna DJ Pedithe Full Video Song From DJ
                        Tillu Movie. #DJTillu #TilluAnnaDJPedithe #RamMiriyala #Siddhu
                        #NehaShetty #VimalKrishna Singer: Ram Miriyala Lyrics: Kasarla...
                      </h1>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="tr-content_bottom">
              <div className="d-flex flex-row">
                <div className="tr-vid tr-preview">
                  <iframe
                    width={220}
                    height={170}
                    src="https://www.youtube.com/embed/yGUKYKSJJc8"
                    title="YouTube video player"
                    frameBorder={0}
                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                    allowFullScreen="true"
                  />
                  <div className="tr-info">
                    <div className="tr-video_text">
                      <h1 className="tr-video_heading">
                        Tum Tum - Lyric Video | Enemy (Tamil) | Vishal,Arya | Anand
                        Shankar | Vinod Kumar | Thaman S
                      </h1>
                      <h1 className="tr-para_text"> Divo Music 146M views 3 months ago </h1>
                      <h1 className="tr-video_bottom_text">
                        {" "}
                        Watch &amp; Enjoy Tillu Anna DJ Pedithe Full Video Song From DJ
                        Tillu Movie. #DJTillu #TilluAnnaDJPedithe #RamMiriyala #Siddhu
                        #NehaShetty #VimalKrishna Singer: Ram Miriyala Lyrics: Kasarla...
                      </h1>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </>
          

        );
    }
}

export default UtubeTrendingPage;