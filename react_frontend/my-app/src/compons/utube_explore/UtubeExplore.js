import React, { Component } from 'react';
import "./UtubeExplore.css"

class UtubeExplore extends Component {
    render() {
        return (
            <>
  <div className="topbar">
    <div className="left">
      <i className="fas fa-bars" />
      <div className="logo">
        <img src="image.png" class="pl-logo" />
        <span class="logo-title"> My Tube </span>
      </div>
    </div>
    <div className="middle">
      <div className="search">
        <input type="text" name="" id="" placeholder="Search" />
        <div className="searchbtn">
          <i className="fa fa-search" />
        </div>
      </div>
      <i className="fas fa-microphone" />
    </div>
    <div className="right">
      <i className="fas fa-video" />
      <i className="fas fa-th" />
      <i className="fas fa-bell" />
      <img className="profile" src="https://picsum.photos/200" alt="" />
    </div>
  </div>
  <div className="sidebar">
    <div className="section">
      <ul>
        <li>
          {" "}
          <i className="fas fa-home" /> Home
        </li>
        <li className="active">
          <i className="fas fa-compass" />
          Explore
        </li>
        <li>
          <i className="fab fa-youtube-square" />
          Subscriptions
        </li>
      </ul>
    </div>
    <div className="section">
      <ul>
        <li>
          <i className="fas fa-play" />
          Library
        </li>
        <li>
          <i className="fas fa-history" />
          History
        </li>
        <li>
          <i className="fab fa-youtube" />
          Your videos
        </li>
      </ul>
    </div>
  </div>
  <div className="exp-content">
    <div className="d-flex flex-row justify-content-center">
      <div className="explore-container">
        <p>
          <img
            src="https://media.istockphoto.com/vectors/fire-icon-design-red-flame-sign-ignite-dangerous-vector-symbol-vector-id1279525967?k=20&m=1279525967&s=170667a&w=0&h=frivjHksG2cp0ckj62ZXFGGYVvjyhpcMZq1CIVVhJXk="
            className="exp-icon-explore"
          />
        </p>
        <h1 className="explore-content">Trending</h1>
      </div>
      <div className="explore-container">
        <p>
          <img
            src="https://previews.123rf.com/images/blankstock/blankstock1808/blankstock180800134/105629493-music-note-sign-icon-musical-symbol-colorful-button-with-icon-geometric-elements-vector.jpg"
            className="exp-icon-explore"
          />
        </p>
        <h1 className="explore-content">Music</h1>
      </div>
      <div className="explore-container">
        <p>
          <img
            src="https://t3.ftcdn.net/jpg/02/16/36/66/360_F_216366647_RLE3hDL0DMNENQFnwB34lln0ybl7DVEV.jpg"
            className="exp-icon-explore"
          />
        </p>
        <h1 className="explore-content">Learning</h1>
      </div>
      <div className="explore-container">
        <p>
          <img
            src="https://www.nicepng.com/png/detail/107-1070734_png-to-movie-lets-go-to-the-movies.png"
            className="exp-icon-explore"
          />
        </p>
        <h1 className="explore-content">Movies</h1>
      </div>
      <div className="explore-container">
        <p>
          <img
            src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS1CdD_gyVeiVrPLNztqby6hLxA4tzXYIga8TePX4KdOHPjD0lgfByV2LrPQzewQWm7wlU&usqp=CAU"
            className="exp-icon-explore"
          />
        </p>
        <h1 className="explore-content">Sports</h1>
      </div>
    </div>
  </div>
  <div className="exp-content_bottom">
    <div className="d-flex flex-row">
      <div className="exp-vid">
        <iframe
          width={220}
          height={170}
          src="https://www.youtube.com/embed/M-954V9LORI"
          title="YouTube video player"
          frameBorder={0}
          allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
          allowFullScreen="true"
        />
        </div>
      
          <div className="exp-video_text">
            <h1 className="exp-video_heading">
              #TilluAnnaDJPedithe Full Video Song |DJ Tillu Songs |Siddhu,Neha
              Shetty |Vimal Krishna |Ram Miriyala
            </h1>
            <h1 className="exp-para_text">
              {" "}
              Aditya Music 2.5M views 13 hours ago{" "}
            </h1>
            <h1 className="exp-video_bottom_text">
              Watch &amp; Enjoy Tillu Anna DJ Pedithe Full Video Song From DJ
              Tillu Movie. #DJTillu #TilluAnnaDJPedithe #RamMiriyala #Siddhu
              #NehaShetty #VimalKrishna Singer: Ram Miriyala Lyrics: Kasarla...
            </h1>
        
        </div>
     
    </div>
  </div>
  <div className="exp-content_bottom">
    <div className="d-flex flex-row">
      <div className="exp-vid exp-preview">
        <iframe
          width={220}
          height={170}
          src="https://www.youtube.com/embed/yGUKYKSJJc8"
          title="YouTube video player"
          frameBorder={0}
          allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
          allowFullScreen="true"
        />
        </div>
     
          <div className="exp-video_text">
            <h1 className="exp-video_heading">
              Tum Tum - Lyric Video | Enemy (Tamil) | Vishal,Arya | Anand
              Shankar | Vinod Kumar | Thaman S
            </h1>
            <h1 className="exp-para_text"> Divo Music 146M views 3 months ago </h1>
            <h1 className="exp-video_bottom_text">
              {" "}
              Watch &amp; Enjoy Tillu Anna DJ Pedithe Full Video Song From DJ
              Tillu Movie. #DJTillu #TilluAnnaDJPedithe #RamMiriyala #Siddhu
              #NehaShetty #VimalKrishna Singer: Ram Miriyala Lyrics: Kasarla...
            </h1>
          
     
      </div>
    </div>
  </div>
  <div className="exp-content_bottom">
    <div className="d-flex flex-row">
      <div className="exp-vid exp-preview">
        <iframe
          width={220}
          height={170}
          src="https://www.youtube.com/embed/yGUKYKSJJc8"
          title="YouTube video player"
          frameBorder={0}
          allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
          allowFullScreen="true"
        />
        </div>
     
          <div className="exp-video_text">
            <h1 className="exp-video_heading">
              Tum Tum - Lyric Video | Enemy (Tamil) | Vishal,Arya | Anand
              Shankar | Vinod Kumar | Thaman S
            </h1>
            <h1 className="exp-para_text"> Divo Music 146M views 3 months ago </h1>
            <h1 className="exp-video_bottom_text">
              {" "}
              Watch &amp; Enjoy Tillu Anna DJ Pedithe Full Video Song From DJ
              Tillu Movie. #DJTillu #TilluAnnaDJPedithe #RamMiriyala #Siddhu
              #NehaShetty #VimalKrishna Singer: Ram Miriyala Lyrics: Kasarla...
            </h1>
          
     
      </div>
    </div>
  </div>
  <div className="exp-content_bottom">
    <div className="d-flex flex-row">
      <div className="exp-vid exp-preview">
        <iframe
          width={220}
          height={170}
          src="https://www.youtube.com/embed/yGUKYKSJJc8"
          title="YouTube video player"
          frameBorder={0}
          allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
          allowFullScreen="true"
        />
        </div>
     
          <div className="exp-video_text">
            <h1 className="exp-video_heading">
              Tum Tum - Lyric Video | Enemy (Tamil) | Vishal,Arya | Anand
              Shankar | Vinod Kumar | Thaman S
            </h1>
            <h1 className="exp-para_text"> Divo Music 146M views 3 months ago </h1>
            <h1 className="exp-video_bottom_text">
              {" "}
              Watch &amp; Enjoy Tillu Anna DJ Pedithe Full Video Song From DJ
              Tillu Movie. #DJTillu #TilluAnnaDJPedithe #RamMiriyala #Siddhu
              #NehaShetty #VimalKrishna Singer: Ram Miriyala Lyrics: Kasarla...
            </h1>
          
     
      </div>
    </div>
  </div>
 
</>

        );
    }
}

export default UtubeExplore;