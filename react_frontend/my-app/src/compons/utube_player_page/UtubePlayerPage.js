import React, { Component } from "react";
import "./UtubePlayerPage.css";

class UtubePlayerPage extends Component {
  render() {
    return (
      <>
  <div className="topbar">
    <div className="left">
      <i className="fas fa-bars" />
      <div className="logo">
        <img src="image.png" class="pl-logo" />
        <span class="logo-title"> My Tube </span>
      </div>
    </div>
    <div className="middle">
      <div className="search">
        <input type="text" name="" id="" placeholder="Search" />
        <div className="searchbtn">
          <i className="fa fa-search" />
        </div>
      </div>
      <i className="fas fa-microphone" />
    </div>
    <div className="right">
      <i className="fas fa-video" />
      <i className="fas fa-th" />
      <i className="fas fa-bell" />
      <img className="profile" src="https://picsum.photos/200" alt="" />
    </div>
  </div>
  <div className="sidebar">
    <div className="section">
      <ul>
        <li>
          <i className="fas fa-home" /> Home
        </li>
        <li>
          <i className="fas fa-compass" />
          Explore
        </li>
        <li>
          <i className="fab fa-youtube-square" />
          Subscriptions
        </li>
      </ul>
    </div>
    <div className="section">
      <ul>
        <li>
          <i className="fas fa-play" />
          Library
        </li>
        <li>
          <i className="fas fa-history" />
          History
        </li>
        <li>
          <i className="fab fa-youtube" />
          Your videos
        </li>
      </ul>
    </div>
  </div>
  <div className="pl-content">
    <div className="d-flex flex-row">
      <div className="d-flex flex-column">
        <div className="vid preview">
          <iframe
            width={520}
            height={370}
            src="https://www.youtube.com/embed/M-954V9LORI"
            title="YouTube video player"
            frameBorder={0}
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen="true"
          >
            &lt;h1&gt; Mohan &lt;/h1&gt;
          </iframe>
        </div>
        <div className="info">
          <div className="pl-video_text">
            <h1 className="pl-video_heading">
              #TilluAnnaDJPedithe Full Video Song |DJ Tillu Songs |Siddhu, Neha
              Shetty |Vimal Krishna |Ram Miriyala
            </h1>
          </div>
        </div>
        <div className="d-flex flex-row pl-icons-sec">
          <p className="pl-views">37,686,753 views</p>
          <div className="pl-icons">
            <i className="fas fa-thumbs-up"> 4.6M likes</i>
            <i className="fas fa-thumbs-down"> Dislike</i>
          </div>
        </div>
        <hr className="pl-line" />
        <div className="d-flex flex-column">
          <h1 className="pl-comment"> Comments </h1>
          <div className="pl-comment-sec">
            <img
              src="https://images.pexels.com/photos/771742/pexels-photo-771742.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"
              className="profile-size"
            />
            <span> Harish</span>
            <p className="pl-user-comment">
              This song will rocks on DJ till next year In Indian weddings!
            </p>
          </div>
          <div className="pl-comment-sec">
            <img
              src="https://i.pinimg.com/originals/19/cf/78/19cf789a8e216dc898043489c16cec00.jpg"
              className="profile-size"
            />
            <span> Fayal Clips</span>
            <p className="pl-user-comment">
              Ram Miriyala is the singer, lyricist, and composer of this song,
              which will propel his career in the Telugu film industry to new
              heights.
            </p>
          </div>
          <div className="pl-comment-sec">
            <img
              src="https://1.bp.blogspot.com/-0ZUMPsBahSo/X0vuBttwtWI/AAAAAAAAdwM/_0Nuxi-PWUsgTsLdAmGZqILPiJf7N2bdACLcBGAsYHQ/s1600/best%2Bdp%2Bfor%2Bwhatsapp%2B%25281%2529.jpg"
              className="profile-size"
            />
            <span> Ansari Jaggu </span>
            <p className="pl-user-comment">
              I respect everyone who were involved in this, seriously the best
              piece that l' ve ever seen on youtube ,Hats of to well All!
            </p>
          </div>
          <a className="pl-comment" href="">
            {" "}
            See More{" "}
          </a>
        </div>
      </div>
      <div className="pl-content_bottom">
        <div className="d-flex flex-column">
          <div className="d-flex flex-row">
            <div className="vid preview">
              <iframe
                width={170}
                height={140}
                src="https://www.youtube.com/embed/yGUKYKSJJc8"
                title="YouTube video player"
                frameBorder={0}
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                allowFullScreen=""
              />
            </div>
            <div className="rt-video_text">
              <h1 className="rt-video_heading">
                Tum Tum - Lyric Video | Enemy (Tamil) | Vishal,Arya | Anand
                Shankar | Vinod Kumar | Thaman S
              </h1>
              <h1 className="rt-para_text">
                {" "}
                Divo Music 146M views 3 months ago{" "}
              </h1>
              <h1 className="rt-video_bottom_text">
                {" "}
                Watch &amp; Enjoy Tillu Anna DJ Pedithe Full Video Song From DJ
                Tillu Movie.{" "}
              </h1>
            </div>
          </div>
          <div className="d-flex flex-row">
            <div className="vid preview">
              <iframe
                width={170}
                height={140}
                src="https://www.youtube.com/embed/yGUKYKSJJc8"
                title="YouTube video player"
                frameBorder={0}
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                allowFullScreen=""
              />
            </div>
            <div className="rt-video_text">
              <h1 className="rt-video_heading">
                Tum Tum - Lyric Video | Enemy (Tamil) | Vishal,Arya | Anand
                Shankar | Vinod Kumar | Thaman S
              </h1>
              <h1 className="rt-para_text">
                {" "}
                Divo Music 146M views 3 months ago{" "}
              </h1>
              <h1 className="rt-video_bottom_text">
                {" "}
                Watch &amp; Enjoy Tillu Anna DJ Pedithe Full Video Song From DJ
                Tillu Movie.{" "}
              </h1>
            </div>
          </div>
          <div className="d-flex flex-row">
            <div className="vid preview">
              <iframe
                width={170}
                height={140}
                src="https://www.youtube.com/embed/yGUKYKSJJc8"
                title="YouTube video player"
                frameBorder={0}
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                allowFullScreen=""
              />
            </div>
            <div className="rt-video_text">
              <h1 className="rt-video_heading">
                Tum Tum - Lyric Video | Enemy (Tamil) | Vishal,Arya | Anand
                Shankar | Vinod Kumar | Thaman S
              </h1>
              <h1 className="rt-para_text">
                {" "}
                Divo Music 146M views 3 months ago{" "}
              </h1>
              <h1 className="rt-video_bottom_text">
                {" "}
                Watch &amp; Enjoy Tillu Anna DJ Pedithe Full Video Song From DJ
                Tillu Movie.{" "}
              </h1>
            </div>
          </div>
          <div className="d-flex flex-row">
            <div className="vid preview">
              <iframe
                width={170}
                height={140}
                src="https://www.youtube.com/embed/yGUKYKSJJc8"
                title="YouTube video player"
                frameBorder={0}
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                allowFullScreen=""
              />
            </div>
            <div className="rt-video_text">
              <h1 className="rt-video_heading">
                Tum Tum - Lyric Video | Enemy (Tamil) | Vishal,Arya | Anand
                Shankar | Vinod Kumar | Thaman S
              </h1>
              <h1 className="rt-para_text">
                {" "}
                Divo Music 146M views 3 months ago{" "}
              </h1>
              <h1 className="rt-video_bottom_text">
                {" "}
                Watch &amp; Enjoy Tillu Anna DJ Pedithe Full Video Song From DJ
                Tillu Movie.{" "}
              </h1>
            </div>
          </div>
          <div className="d-flex flex-row">
            <div className="vid preview">
              <iframe
                width={170}
                height={140}
                src="https://www.youtube.com/embed/yGUKYKSJJc8"
                title="YouTube video player"
                frameBorder={0}
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                allowFullScreen=""
              />
            </div>
            <div className="rt-video_text">
              <h1 className="rt-video_heading">
                Tum Tum - Lyric Video | Enemy (Tamil) | Vishal,Arya | Anand
                Shankar | Vinod Kumar | Thaman S
              </h1>
              <h1 className="rt-para_text">
                {" "}
                Divo Music 146M views 3 months ago{" "}
              </h1>
              <h1 className="rt-video_bottom_text">
                {" "}
                Watch &amp; Enjoy Tillu Anna DJ Pedithe Full Video Song From DJ
                Tillu Movie.{" "}
              </h1>
            </div>
          </div>
          <div className="d-flex flex-row">
            <div className="vid preview">
              <iframe
                width={170}
                height={140}
                src="https://www.youtube.com/embed/yGUKYKSJJc8"
                title="YouTube video player"
                frameBorder={0}
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                allowFullScreen=""
              />
            </div>
            <div className="rt-video_text">
              <h1 className="rt-video_heading">
                Tum Tum - Lyric Video | Enemy (Tamil) | Vishal,Arya | Anand
                Shankar | Vinod Kumar | Thaman S
              </h1>
              <h1 className="rt-para_text">
                {" "}
                Divo Music 146M views 3 months ago{" "}
              </h1>
              <h1 className="rt-video_bottom_text">
                {" "}
                Watch &amp; Enjoy Tillu Anna DJ Pedithe Full Video Song From DJ
                Tillu Movie.{" "}
              </h1>
            </div>
          </div>
          <div className="d-flex flex-row">
            <div className="vid preview">
              <iframe
                width={170}
                height={140}
                src="https://www.youtube.com/embed/yGUKYKSJJc8"
                title="YouTube video player"
                frameBorder={0}
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                allowFullScreen=""
              />
            </div>
            <div className="rt-video_text">
              <h1 className="rt-video_heading">
                Tum Tum - Lyric Video | Enemy (Tamil) | Vishal,Arya | Anand
                Shankar | Vinod Kumar | Thaman S
              </h1>
              <h1 className="rt-para_text">
                {" "}
                Divo Music 146M views 3 months ago{" "}
              </h1>
              <h1 className="rt-video_bottom_text">
                {" "}
                Watch &amp; Enjoy Tillu Anna DJ Pedithe Full Video Song From DJ
                Tillu Movie.{" "}
              </h1>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</>

    );
  }
}

export default UtubePlayerPage;
